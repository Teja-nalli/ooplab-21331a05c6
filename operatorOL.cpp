#include<iostream>
using namespace std;

class Complex {
private:
    double real;
    double imag;

public:
    Complex(double r = 0, double i = 0) : real(r), imag(i) {}

    Complex operator*(const Complex& other) const {
        double r = real * other.real - imag * other.imag;
        double i = real * other.imag + imag * other.real;
        return Complex(r, i);
    }

    void print() const {
        cout << "(" << real << ", " << imag << "i)" << endl;
    }
};

int main() {
    Complex a(2, 3);
    Complex b(4, -1);

    Complex c = a * b;
    c.print();
}
