#include<iostream>
#include "boxArea.h"
#include "boxVolume.h"

using namespace std;

int main() {
    int l,b,h;
    cout<<"Enter length ,breadth and height of the box: "<<endl;
    cin>>l>>b>>h;
    cout<< "Area of the box: "<<boxArea(l, b, h)<<endl;
    cout<<"Volume of the box: "<<boxVolume(l, b, h)<<endl;
}
