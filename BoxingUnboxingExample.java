public class BoxingUnboxingExample { public static void main(String[] args) { int num1 = 10;
Integer num2 = Integer.valueOf(num1);
System.out.println("num1 = " + num1);
System.out.println("num2 = " + num2); Integer num3 = Integer.valueOf(20); int num4 = num3.intValue();
System.out.println("num3 = " + num3); System.out.println("num4 = " + num4);
double num5 = 3.14; Double num6 = num5;
System.out.println("num5 = " + num5);
System.out.println("num6 = " + num6); Integer num7 = Integer.valueOf(30); int num8 = num7;
System.out.println("num7 = " + num7);
System.out.println("num8 = " + num8); }
}
