#include<iostream>
using namespace std; 
template<typename T>
T add(T a, T b) { 
    return a + b;
}
template<typename T> 
class Math { 
    private:
    T value;
    public:
        Math(T value) { 
            this->value = value;
        }
    T add(T a) { 
        return value + a;
    }
}; 
int main() { 
    int result1 = add<int>(2, 3); 
    cout <<result1 << endl; 
    double result2 = add<double>(2.5, 3.5);
    cout <<result2 << endl; 
    Math<int> math1(5); 
    int result3 = math1.add(10); 
    cout <<result3 << endl; 
    Math<double> math2(2.5); 
    double result4 = math2.add(3.5);
    cout <<result4 << endl;
}
