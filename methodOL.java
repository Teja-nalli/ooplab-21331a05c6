public class Animal {
    public void makeSound() {
        System.out.println("The animal makes a sound");
    }
}

public class Dog extends Animal {
    public void makeSound(String bark) {
        System.out.println("The dog barks: " + bark);
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myAnimal = new Animal();
        Dog myDog = new Dog();

        myAnimal.makeSound();
        myDog.makeSound();
        myDog.makeSound("BOW");
    }
}
