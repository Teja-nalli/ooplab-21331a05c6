import java.applet.Applet; 
import java.awt.Graphics;

public class AppletDemo extends Applet{
    
    public void paint(Graphics g){ 
        
        g.drawString("Hello Java",120,50);
        
    }
}

/*
 <applet code="AppletDemo",width="300",height="70">
 </applet>
*/
