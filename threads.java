class PrintNumbers extends Thread {
    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("Thread 1: " + i);
        }
    }
}

class PrintLetters extends Thread {
    @Override
    public void run() {
        for (char letter = 'A'; letter <= 'E'; letter++) {
            System.out.println("Thread 2: " + letter);
        }
    }
}


class threads {
    public static void main(String[] args) {

        Thread thread1 = new Thread(new PrintNumbers());
        Thread thread2 = new Thread(new PrintLetters());


        thread1.start();
        thread2.start();
    }
}
