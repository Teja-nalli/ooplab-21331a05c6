#include <iostream>

using namespace std;


int add(int a, int b) {
    return a + b;
}


int add(int a, int b, int c) {
    return a + b + c;
}

double add(double a, double b) {
    return a + b;
}

int main() {

    cout << "Sum of two integers: " << add(2, 3) << endl;
    cout << "Sum of three integers: " << add(2, 3, 4) << endl;
    cout << "Sum of two floating point numbers: " << add(2.5, 3.5) << endl;
}
