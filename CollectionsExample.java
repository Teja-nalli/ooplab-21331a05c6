import java.util.ArrayList;
import java.util.List;

class CollectionsExample {
    public static void main(String[] args) {
        
        List<Integer> numbers = new ArrayList<>();

        
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);

        
        System.out.println("Element at index 0: " + numbers.get(0));
        System.out.println("Element at index 1: " + numbers.get(1));
        System.out.println("Element at index 2: " + numbers.get(2));

        
        numbers.remove(1);
        System.out.println("After removing element at index 1: " + numbers);

        
        boolean contains20 = numbers.contains(20);
        System.out.println("Does the list contain 20? " + contains20);

        
        int size = numbers.size();
        System.out.println("Size of the list: " + size);

        
        System.out.print("List elements: ");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
