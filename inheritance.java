class Base {
    public void publicFunction() {
        System.out.println("This is a public function in the Base class.");
    }

    protected void protectedFunction() {
        System.out.println("This is a protected function in the Base class.");
    }

    private void privateFunction() {
        System.out.println("This is a private function in the Base class.");
    }
}

class PrivateDerived extends Base {
    public void callPrivateFunction() {
        
    }

    public void callProtectedFunction() {
        protectedFunction();
    }

    public void callPublicFunction() {
        publicFunction();
    }
}

class ProtectedDerived extends Base {
    public void callPrivateFunction() {
        
    }

    public void callProtectedFunction() {
        protectedFunction();
    }

    public void callPublicFunction() {
        publicFunction();
    }
}

class PublicDerived extends Base {
    public void callPrivateFunction() {

    }

    public void callProtectedFunction() {
        protectedFunction();
    }

    public void callPublicFunction() {
        publicFunction();
    }
}

public class inheritance {
    public static void main(String[] args) {
        PrivateDerived privateDerived = new PrivateDerived();
        ProtectedDerived protectedDerived = new ProtectedDerived();
        PublicDerived publicDerived = new PublicDerived();

        privateDerived.callPublicFunction();
        protectedDerived.callPublicFunction();
        publicDerived.callPublicFunction();
    }
}
