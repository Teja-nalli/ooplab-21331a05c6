class PrintNum implements Runnable {
    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("Thread 1: " + i);
        }
    }
}

class PrintLet implements Runnable {
    @Override
    public void run() {
        for (char letter = 'A'; letter <= 'E'; letter++) {
            System.out.println("Thread 2: " + letter);
        }
    }
}

class runnableThread {
    public static void main(String[] args) {
        
        Thread thread1 = new Thread(new PrintNum());
        Thread thread2 = new Thread(new PrintLet());

        
        thread1.start();
        thread2.start();
    }
}
