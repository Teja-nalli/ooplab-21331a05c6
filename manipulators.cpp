#include<iostream>
#include<iomanip>
using namespace std;

void endlFun() {
    cout<<"endl use"<<endl;
    cout<<"line1"<<endl<<"line2"<<endl;
    cout<<endl;
}

void endsFun(){
    cout<<"ends use"<<endl;
    cout<<'a'<<ends;
    cout<<'b'<<ends;
    cout<<'c'<<endl;
    cout<<endl;
}

void flushFun(){
    cout<<"flush use"<<endl;
    cout<<"example of flush"<<flush<<endl;
    cout<<endl;
}

void setwFun(){
    cout<<"setw use"<<endl;
    cout<<setw(10)<<23<<endl;
    cout<<setw(10)<<03<<endl;
    cout<<endl;
}

void setfillFun(){
    cout<<"setfill use"<<endl;
    cout<<setfill('0')<<setw(10)<<23<<endl;
    cout<<setfill('*')<<setw(10)<<03<<endl;
    cout<<endl;
}

void setprecisionFun(){
    cout<<"setprecision use"<<endl;
    cout<<setprecision(5)<<67.45708<<endl;
    cout<<setprecision(7)<<57.0989109<<endl;
    cout<<endl;
}

int main(){
    endlFun();
    endsFun();
    flushFun();
    setwFun();
    setfillFun();
    setprecisionFun();
}

