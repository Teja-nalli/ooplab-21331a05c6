#include <iostream>
using namespace std;


class Animal {
public:
    void eat() {
        cout << "Animal is eating." << endl;
    }
};


class Dog : public Animal {
public:
    void bark() {
        cout << "Dog is barking." << endl;
    }
};


class Labrador : public Animal, public Dog {
public:
    void color() {
        cout << "Labrador is golden in color." << endl;
    }
};


class Cat : public Animal {
public:
    void meow() {
        cout << "Cat is meowing." << endl;
    }
};


class Lion : public Cat {
public:
    void roar() {
        cout << "Lion is roaring." << endl;
    }
};

int main() {
    
    Dog d;
    d.eat();
    d.bark();


    Labrador l;
    l.eat();
    l.bark();
    l.color();


    Cat c;
    c.eat();
    c.meow();


    Lion li;
    li.eat();
    li.meow();
    li.roar();

}
