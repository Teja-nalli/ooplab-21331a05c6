interface Shape {
    void draw();
}

class Circle implements Shape {
    private double radius;

    public Circle(double r) {
        radius = r;
    }

    public void draw() {
        System.out.println("Drawing circle with radius " + radius);
    }
}

class Square implements Shape {
    private double side;

    public Square(double s) {
        side = s;
    }

    public void draw() {
        System.out.println("Drawing square with side " + side);
    }
}

public class pureAbstract {
    public static void main(String[] args) {
        Circle circle = new Circle(5.0);
        Square square = new Square(3.0);

        Shape[] shapes = new Shape[2];
        shapes[0] = circle;
        shapes[1] = square;

        for (int i = 0; i < 2; i++) {
            shapes[i].draw();
        }
    }
}
