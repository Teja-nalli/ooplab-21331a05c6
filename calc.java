import java.util.*;
import java.lang.*;

class Calculator {
    public static void main (String[] args) {
        int x;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter two integers A and B: ");
        int a = input.nextInt();
        int b = input.nextInt();
        System.out.println("1: Addition\n2: Subtraction\n3: Multiplication\n4: Division\n5: Modulo Division");
        System.out.println("Enter an operation to be performed");
        int c = input.nextInt();
        if (c==1) {
            x = a+b;
            System.out.println("Addition of " + a + " and " + b +" is: " + x);
        }
        else if (c==2) {
            x = a-b;
            System.out.println("Subtraction of " + a + " and "+ b + " is: " + x);
        }
        else if (c==3) {
            x = a*b;
            System.out.println("Multiplication of " + a + " and "+ b + " is: " + x);
        }
        else if (c==4) {
            x = a/b;
            System.out.println("Division of " + a + " and "+ b + " is: " + x);
        }
        else if (c==5) {
            x = a%b;
            System.out.println("Modulo Division of " + a + " and "+ b + " is: " + x);
        }
        else {
            System.out.println("Invalid option Entered");
        }
    }
}
