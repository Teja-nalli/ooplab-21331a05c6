interface A {
    public void print();
}

class B implements A {
    public void print() {
        System.out.println("This is class B.");
    }
}

class C implements A {
    public void print() {
        System.out.println("This is class C.");
    }
}

class D implements A {
    B b = new B();
    C c = new C();
    public void print() {
        b.print();
        c.print();
    }
}

class diamond{
    public static void main(String[] args) {
        D d = new D();
        d.print();
    }
}
