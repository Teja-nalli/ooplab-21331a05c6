#include<iostream>
using namespace std;

class Box {
    public:
        float length, breadth, height;
        void boxArea(float l, float b, float h) {
            length = l;
            breadth = b;
            height = h;
            cout<<"Area of the box is: "<<2*(l*b+b*h+h*l)<<endl;
        }
        
        void boxVolume(float l, float b, float h);
        
        friend void displayBoxDimensions(Box b);
        
        inline void displayWelcomeMessage();
};

void Box :: boxVolume(float l, float b, float h) {
    cout<<"Volume of the Box is: "<<l*b*h<<endl;
    cout<<endl;
}

void displayBoxDimensions(Box obj) {
    cout<<"Length of the Box: "<<obj.length<<endl;
    cout<<"Breadth of the Box: "<<obj.breadth<<endl;
    cout<<"Height of the Box: "<<obj.height<<endl;
    cout<<endl;
}

inline void Box :: displayWelcomeMessage() {
    cout<<"Welcome to the program"<<endl;
    cout<<endl;
}

int main() {
    float l,b,h;
    
    Box gift;
    
    gift.displayWelcomeMessage();
    
    cout<<"Enter the length, breadth and volume of the box: "<<endl;
    cin>>l>>b>>h;
    cout<<endl;
    
    displayBoxDimensions(gift);
    
    gift.boxArea(l, b, h);
    gift.boxVolume(l, b, h);
}
