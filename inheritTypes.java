class Animal {
    public void eat() {
        System.out.println("Animal is eating.");
    }
}

class Dog extends Animal {
    public void bark() {
        System.out.println("Dog is barking.");
    }
}

class Labrador extends Animal, Dog {
    public void color() {
        System.out.println("Labrador is golden in color.");
    }
}

class Cat extends Animal {
    public void meow() {
        System.out.println("Cat is meowing.");
    }
}

class Lion extends Cat {
    public void roar() {
        System.out.println("Lion is roaring.");
    }
}

public class Main {
    public static void main(String[] args) {
        
        Dog d = new Dog();
        d.eat();
        d.bark();
        
        Labrador l = new Labrador();
        l.eat();
        l.bark();
        l.color();
        
        Cat c = new Cat();
        c.eat();
        c.meow();
        
        Lion li = new Lion();
        li.eat();
        li.meow();
        li.roar();
    }
}
