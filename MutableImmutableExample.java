final class ImmutablePerson { 
    private final String name; 
    private final int age;
    public ImmutablePerson(String name, int age) { 
        this.name = name; this.age = age;
    }
    public String getName() { 
        return name;
    }
    public int getAge() { 
        return age;
    }
    public ImmutablePerson withName(String name) { 
        return new ImmutablePerson(name, this.age);
    }
    public ImmutablePerson withAge(int age) { 
        return new ImmutablePerson(this.name, age); 
    }
}

class MutablePerson { 
    private String name; 
    private int age;
    public MutablePerson(String name, int age) { 
        this.name = name; this.age = age;
    }
    public String getName() { 
        return name;
    }
    public void setName(String name) { 
        this.name = name;
    }
    public int getAge() { 
        return age;
    }
    public void setAge(int age) { 
        this.age = age;
    }
}

public class MutableImmutableExample { 
    public static void main(String[] args) {
        ImmutablePerson immutablePerson = new ImmutablePerson("John", 30);
        System.out.println("Immutable person: " + immutablePerson.getName() + ", " + immutablePerson.getAge());
        ImmutablePerson newImmutablePerson = immutablePerson.withName("Mike").withAge(35);
        System.out.println("New immutable person: " + newImmutablePerson.getName() + ", " + newImmutablePerson.getAge());
        MutablePerson mutablePerson = new MutablePerson("John", 30);
        System.out.println("Mutable person: " + mutablePerson.getName() + ", " + mutablePerson.getAge()); 
        mutablePerson.setName("Mike"); mutablePerson.setAge(35);
        System.out.println("New mutable person: " + mutablePerson.getName() + ", " + mutablePerson.getAge()); 
    }
}
        
