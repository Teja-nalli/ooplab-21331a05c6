import java.util.*;

class studentConst {
    String fullname;
    String rollno;
    double cgpa;
    String collegename;
    int collegecode;

    studentConst() {
        collegename = "MVGR";
        collegecode = 33;
    }

    studentConst(String fname, String rno, double c){
        fullname = fname;
        rollno = rno;
        cgpa = c;
    }
    void displaymsg(){
        System.out.println("College Name: "+collegename);
        System.out.println("College Code: "+collegecode);
        System.out.println("Full Name: "+fullname);
        System.out.println("Roll No: "+ rollno);
        System.out.println("CGPA: "+cgpa);
    }

    protected void finalize(){
        System.out.println("Destructed");
    }

    public static void main(String[] args){
        studentConst obj = new studentConst("Rahul", "@Z10", 8.75);
        studentConst obj = new studentConst();
        obj.displaymsg();
    }
}
