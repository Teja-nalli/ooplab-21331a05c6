#include <iostream>
using namespace std;

class Base {
public:
  void publicFunction() {
    cout << "This is a public function in the Base class." <<endl;
  }

protected:
  void protectedFunction() {
    cout << "This is a protected function in the Base class." << endl;
  }

private:
  void privateFunction() {
    cout << "This is a private function in the Base class." << endl;
  }
};


class PrivateDerived : private Base {
public:
  void callPrivateFunction() {
    
  }

  void callProtectedFunction() {
    protectedFunction();
  }

  void callPublicFunction() {
    publicFunction();
  }
};


class ProtectedDerived : protected Base {
public:
  void callPrivateFunction() {
  }

  void callProtectedFunction() {
    protectedFunction();
  }

  void callPublicFunction() {
    publicFunction();
  }
};


class PublicDerived : public Base {
public:
  void callPrivateFunction() {
  }

  void callProtectedFunction() {
    protectedFunction();
  }

  void callPublicFunction() {
    publicFunction();
  }
};

int main() {
  PrivateDerived privateDerived;
  ProtectedDerived protectedDerived;
  PublicDerived publicDerived;

  privateDerived.callPublicFunction();
  protectedDerived.callPublicFunction();
  publicDerived.callPublicFunction();
}
