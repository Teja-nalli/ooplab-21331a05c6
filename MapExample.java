import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        
        Map<String, Integer> studentScores = new HashMap<>();

        
        studentScores.put("John", 85);
        studentScores.put("Alice", 92);
        studentScores.put("Bob", 78);
        studentScores.put("Emily", 95);
        studentScores.put("David", 88);

        
        int score = studentScores.get("Alice");
        System.out.println("Alice's score: " + score);

        
        studentScores.put("Bob", 82);
        score = studentScores.get("Bob");
        System.out.println("Bob's updated score: " + score);

        
        boolean exists = studentScores.containsKey("Emily");
        System.out.println("Emily's score exists: " + exists);

        
        studentScores.remove("John");
        exists = studentScores.containsKey("John");
        System.out.println("John's score exists after removal: " + exists);

        
        System.out.println("Student scores:");
        for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
            String student = entry.getKey();
            int studentScore = entry.getValue();
            System.out.println(student + ": " + studentScore);
        }

        
        studentScores.clear();
        System.out.println("Number of entries after clearing: " + studentScores.size());
    }
}
