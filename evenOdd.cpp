#include<iostream>
using namespace std;

void evenodd(int n) {
    if (n%2 == 0) {
        cout<<"Given number "<<n<<" is an Even number"<<endl;
    }
    else {
        cout<<"Given number "<<n<<" is an Odd number"<<endl;
    }
}

int main() {
    int num;
    cout<<"Enter a number: "<<endl;
    cin>>num;
    evenodd(num);
}
