import java.util.*;

public class ExceptionHandlingExample {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        try {
            System.out.print("Enter the numerator: ");
            int numerator = input.nextInt();

            System.out.print("Enter the denominator: ");
            int denominator = input.nextInt();

            int result = numerator / denominator;

            System.out.println("Result: " + result);

        }
        catch (ArithmeticException e) {
            System.out.println("Exception caught: " + e.getMessage());
            System.out.println("Cannot divide by zero!");
        } 
        catch (InputMismatchException e) {
            System.out.println("Exception caught: " + e.getMessage());
            System.out.println("Invalid input! Please enter integers only.");
        } 
    }
}
