#include<iostream>
using namespace std;

int main() {
    char o;
    int a,b;
    cout<<"Enter two integers A and B: "<<endl;
    cin>>a>>b;
    cout<<"Enter an operator in '+, -, *, /, %': "<<endl;
    cin>>o;
    switch(o) {
        case '+':
            cout<<"Addition of "<<a<<" and "<<b<<" is: "<<a+b<<endl;
            break;
        case '-':
            cout<<"Subtraction of "<<a<<" and "<<b<<" is: "<<a-b<<endl;
            break;
        case '*':
            cout<<"Multiplication of "<<a<<" and "<<b<<" is: "<<a*b<<endl;
            break;
        case '/':
            cout<<"Division of "<<a<<" and "<<b<<" is: "<<a/b<<endl;
            break;
        case '%':
            cout<<"Modulo Division of "<<a<<" and "<<b<<" is: "<<a%b<<endl;
            break;
        default:
            cout<<"Entered operator is Invalid"<<endl;
            break;
    }
}
