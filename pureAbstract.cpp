#include <iostream>
using namespace std;
class Shape {
public:
    virtual void display() = 0;
};

class Circle : public Shape {
private:
    double radius;

public:
    Circle(double r) : radius(r) {}

    void display() { 
        cout << "Circle with radius " << radius << endl; 
    }
};

class Rectangle : public Shape {
private:
    double width;
    double height;

public:
    Rectangle(double w, double h) : width(w), height(h) {}

    void display() { 
        cout << "Rectangle with width " << width << " and height " << height << endl; 
    }
};

int main() {
    Circle circle(5.0);
    Rectangle rectangle(3.0, 4.0);

    Shape* shapes[2];
    shapes[0] = &circle;
    shapes[1] = &rectangle;

    for (int i = 0; i < 2; i++) {
        shapes[i]->display();
    }

    return 0;
}






