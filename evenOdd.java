import java.util.*;
import java.lang.*;

class Evenodd {
    public static void main (String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.println("Enter a number: ");
        int num = input.nextInt();
        evenOrOdd(n);
    }

    static void evenOrOdd(int n) {
        if (n%2 == 0) {
            System.out.println("Given number " + n + " is an Even number");
        }
        else {
            System.out.println("Given number " + n + " is an Odd number");
        }
    }
}
