import javax.script.*;

public class SimpleBindingsExample {
    public static void main(String[] args) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        // Create a SimpleBindings object
        SimpleBindings bindings = new SimpleBindings();

        // Use the put() method to add variables to the bindings
        bindings.put("name", "John Doe");
        bindings.put("age", 30);

        // Use the get() method to retrieve variables from the bindings
        String name = (String) bindings.get("name");
        int age = (int) bindings.get("age");

        System.out.println("Name: " + name);
        System.out.println("Age: " + age);

        // Use the containsKey() method to check if a variable exists in the bindings
        System.out.println("Contains 'name': " + bindings.containsKey("name"));
        System.out.println("Contains 'salary': " + bindings.containsKey("salary"));

        // Use the putAll() method to add all variables from another bindings object
        SimpleBindings additionalBindings = new SimpleBindings();
        additionalBindings.put("salary", 50000.0);
        additionalBindings.put("city", "New York");
        bindings.putAll(additionalBindings);

        // Use the keySet() method to get a set of all variable names in the bindings
        System.out.println("Variables in the bindings:");
        for (String variableName : bindings.keySet()) {
            System.out.println(variableName + ": " + bindings.get(variableName));
        }

        // Use the remove() method to remove a variable from the bindings
        bindings.remove("age");

        // Use the clear() method to remove all variables from the bindings
        bindings.clear();

        System.out.println("Variables after clearing the bindings:");
        System.out.println("Contains 'name': " + bindings.containsKey("name"));
        System.out.println("Contains 'salary': " + bindings.containsKey("salary"));
    }
}
