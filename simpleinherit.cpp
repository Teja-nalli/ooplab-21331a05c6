#include <iostream>
using namespace std;


class Animal {
public:
    string name;
    int age;
    void move() {
        cout << name << " is moving." << endl;
    }
};



class Cat : public Animal {
public:
    void eat() {
        cout << name << " is Eating." << endl;
    }
};

int main() {
    Cat myCat;
    myCat.name = "Cat";
    myCat.age = 3;



    myCat.move();
    myCat.eat();
}
