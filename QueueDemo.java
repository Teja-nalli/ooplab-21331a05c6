import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {

    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        // Enqueue elements
        queue.offer("Alice");
        queue.offer("Bob");
        queue.offer("Charlie");
        queue.offer("Dave");

        System.out.println("Queue: " + queue);

        // Dequeue an element
        String firstPerson = queue.poll();
        System.out.println("Dequeued: " + firstPerson);
        System.out.println("Updated Queue: " + queue);

        // Peek the front element
        String frontPerson = queue.peek();
        System.out.println("Front Element: " + frontPerson);

        // Display all elements
        System.out.print("Elements in the Queue: ");
        for (String person : queue) {
            System.out.print(person + " ");
        }
    }
}
