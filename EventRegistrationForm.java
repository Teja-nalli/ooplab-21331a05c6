import java.awt.*;
import java.awt.event.*;

class EventRegistrationForm extends Frame {
    private Label nameLabel;
    private TextField nameTextField;
    private Label emailLabel;
    private TextField emailTextField;
    private Label passwordLabel;
    private TextField passwordTextField;
    private Button registerButton;
    private Label msgLabel;

    public EventRegistrationForm() {
        setTitle("Event Registration Form");
        setSize(300, 250);
        setLayout(new GridLayout(5, 2));

        nameLabel = new Label("Name:");
        nameTextField = new TextField(20);

        emailLabel = new Label("Email:");
        emailTextField = new TextField(20);

        passwordLabel = new Label("Password:");
        passwordTextField = new TextField(20);
        passwordTextField.setEchoChar('*');

        registerButton = new Button("Register");
        msgLabel = new Label("");

        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = nameTextField.getText();
                String email = emailTextField.getText();
                String password = passwordTextField.getText();

                if (name.isEmpty() || email.isEmpty() || password.isEmpty()) {
                    msgLabel.setText("Error: All fields are required");
                } else if (!email.endsWith("@gmail.com")) {
                    msgLabel.setText("Error: Email must be a valid Gmail address");
                } else {
                    System.out.println("Name: " + name);
                    System.out.println("Email: " + email);
                    System.out.println("Password: " + password);

                    

                    msgLabel.setText("Registration Successful");
                    nameTextField.setText("");
                    emailTextField.setText("");
                    passwordTextField.setText("");
                }
            }
        });

        addWindowListener(
            new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    dispose();
                }
            }
        );

        add(nameLabel);
        add(nameTextField);
        add(emailLabel);
        add(emailTextField);
        add(passwordLabel);
        add(passwordTextField);
        add(new Label());
        add(registerButton);
        add(msgLabel);

        setVisible(true);
    }

    public static void main(String[] args) {
        new EventRegistrationForm();
    }
}
