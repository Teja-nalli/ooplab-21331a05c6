import java.awt.*;
import java.awt.event.*;

public class LoginDemo extends Frame implements ActionListener {
    private Label lblUsername, lblPassword;
    private TextField txtUsername, txtPassword;
    private Button btnLogin;
    private Label lblMessage;

    public LoginDemo() {
        setTitle("Login Page");
        setSize(300, 200);
        setLayout(new GridLayout(4, 2));

        lblUsername = new Label("Username:");
        add(lblUsername);

        txtUsername = new TextField(20);
        add(txtUsername);

        lblPassword = new Label("Password:");
        add(lblPassword);

        txtPassword = new TextField(20);
        txtPassword.setEchoChar('*');
        add(txtPassword);

        btnLogin = new Button("Login");
        add(btnLogin);

        lblMessage = new Label();
        add(lblMessage);

        btnLogin.addActionListener(this);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnLogin) {
            String username = txtUsername.getText();
            String password = txtPassword.getText();

            if (username.isEmpty() || password.isEmpty()) {
                lblMessage.setText("Please enter both username and password.");
            } else if (username.equals("teja") && password.equals("teja1234")) {
                lblMessage.setText("Login successful. Welcome, " + username + "!");
            } else {
                lblMessage.setText("Invalid username or password.");
            }
        }
    }

    public static void main(String[] args) {
        new LoginDemo();
    }
}
