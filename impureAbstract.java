abstract class animal {
    public void eat() {
        System.out.println("Animal is eating");
    }
    
    public abstract void move();
}

class dog extends animal {
    public void move() {
        System.out.println("Dog is running");
    }
}

public class impureAbstract {
    public static void main(String[] args) {
        dog myDog = new dog();
        myDog.eat();
        myDog.move();
    }
}

    
    
    
    
    
