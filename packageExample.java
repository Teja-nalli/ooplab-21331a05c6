import java.util.Scanner;

import com.example.math.Calculator;

public class packageExample {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter two numbers: ");
        int a = input.nextInt();
        int b = input.nextInt();
        int additionRes = Calculator.add(a, b);
        int subtractionRes = Calculator.subtract(a, b);
        
        System.out.println("Addition of given " + a + " and " + b + " is: " + additionRes);
        System.out.println("Subtraction of given " + a + " and " + b + " is: " + subtractionRes);
    }
}
