#include<iostream>
using namespace std;
class Student{
    public:
        string fullname;
        string rollno;
        double cgpa;
        string collegename;
        int collegecode;
    Student(string fname,string rno,double c,string cname,int ccode){
        fullname=fname;
        rollno=rno;
        cgpa=c;
        collegename=cname;
        collegecode=ccode;
    }
    void display(){
        cout<<"Full Name: "<<fullname<<"\nRoll No: "<<rollno<<"\nCGPA: "<<cgpa<<"\nCollege Name: "<<collegename<<"\nCollege Code: "<<collegecode<<endl;
    }
    ~Student(){
        cout<<"NOT FOUND"<<endl;
    }
};
int main(){
    Student obj("Teja","5C6",8.35,"MVGR",143);
    obj.display();
}
